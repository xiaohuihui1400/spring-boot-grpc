package com.chh.service;

import com.chh.annotation.GrpcService;
import com.chh.util.FileUtils;
import com.example.grpc.HelloRequest;
import com.example.grpc.HelloResponse;
import com.example.grpc.HelloServiceGrpc;
import io.grpc.stub.StreamObserver;
import jakarta.annotation.PostConstruct;

import java.io.File;

/**
 * @author 一碗情深
 */
@GrpcService
public class HelloService extends HelloServiceGrpc.HelloServiceImplBase {

    @PostConstruct
    public void init() {
        HelloRequest helloRequest = HelloRequest.newBuilder()
                .setName("init")
                .setAge(0)
                .addHobbies("read")
                .addHobbies("sing")
                .putTags("sex", "male")
                .build();
        FileUtils.writeFileByte(helloRequest.toByteArray(), "HelloProto");
        System.out.println("HelloProto 二进制文件生成路径：" + System.getProperty("user.dir") + File.separator + "HelloProto");
    }

    @Override
    public void hello(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        System.out.println("HelloService 接收到的参数，name：" + request.getName());

        String greeting = "Hi " + request.getName() + " ,you are " + request.getAge() + " years old" +
                " ,your hobbies is " + (request.getHobbiesList()) + " ,your tags " + request.getTagsMap();

        HelloResponse response = HelloResponse.newBuilder().setGreeting(greeting).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
