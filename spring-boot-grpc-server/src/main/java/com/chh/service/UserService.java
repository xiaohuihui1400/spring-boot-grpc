package com.chh.service;

import com.chh.annotation.GrpcService;
import com.chh.util.FileUtils;
import com.example.grpc.UserRequest;
import com.example.grpc.UserResponse;
import com.example.grpc.UserServiceGrpc;
import io.grpc.stub.StreamObserver;
import jakarta.annotation.PostConstruct;

import java.io.File;

/**
 * @author 一碗情深
 */
@GrpcService
public class UserService extends UserServiceGrpc.UserServiceImplBase {

    @PostConstruct
    public void init() {
        UserRequest userRequest = UserRequest.newBuilder()
                .setName("init")
                .build();
        FileUtils.writeFileByte(userRequest.toByteArray(), "UserProto");
        System.out.println("UserProto 二进制文件生成路径：" + System.getProperty("user.dir") + File.separator + "UserProto");
    }

    @Override
    public void query(UserRequest request, StreamObserver<UserResponse> responseObserver) {
        System.out.println("UserService 接收到的参数，name：" + request.getName());

        UserResponse response = UserResponse.newBuilder()
                .setName("一碗情深")
                .setAge(27)
                .setAddress("https://blog.csdn.net/xiaohuihui1400")
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

}
