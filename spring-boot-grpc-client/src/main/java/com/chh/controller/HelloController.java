package com.chh.controller;

import com.example.grpc.HelloRequest;
import com.example.grpc.HelloResponse;
import com.example.grpc.HelloServiceGrpc;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 一碗情深
 */
@RestController
public class HelloController {

    @Resource
    HelloServiceGrpc.HelloServiceBlockingStub helloServiceBlockingStub;

    /**
     * http://localhost:8098/hello
     */
    @RequestMapping("/hello")
    public String hello() {
        HelloRequest request = HelloRequest.newBuilder()
                .setName("一碗情深")
                .setAge(27)
                .addHobbies("read")
                .addHobbies("sing")
                .putTags("sex", "male")
                .build();
        HelloResponse helloResponse = helloServiceBlockingStub.hello(request);
        String result = helloResponse.getGreeting();
        System.out.println(result);
        return result;
    }

    /**
     * http://localhost:8098/helloProto
     */
    @RequestMapping(value = "/helloProto", consumes = "application/x-protobuf", produces = "application/x-protobuf")
    public HelloResponse helloProto(@RequestBody HelloRequest request) {
        System.out.println("helloProto 接收到的参数，name：" + request.getName() +
                ", age：" + request.getAge() + ", hobbies：" + request.getHobbiesList() +
                ", tags：" + request.getTagsMap());
        HelloRequest helloRequest = HelloRequest.newBuilder()
                .setName("一碗情深")
                .setAge(27)
                .addHobbies("read")
                .addHobbies("sing")
                .putTags("sex", "male")
                .build();
        return helloServiceBlockingStub.hello(helloRequest);
    }

}
