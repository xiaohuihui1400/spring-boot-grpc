package com.chh.controller;

import com.example.grpc.*;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 一碗情深
 */
@RestController
public class UserController {

    @Resource
    UserServiceGrpc.UserServiceBlockingStub userServiceBlockingStub;

    /**
     * http://localhost:8098/user
     */
    @RequestMapping("/user")
    public String user() {
        UserRequest request = UserRequest.newBuilder()
                .setName("名称")
                .build();
        UserResponse userResponse = userServiceBlockingStub.query(request);
        String result = String.format("name:%s, age:%s, address:%s ", userResponse.getName(), userResponse.getAge(), userResponse.getAddress());
        System.out.println(result);
        return result;
    }

    /**
     * http://localhost:8098/userProto
     */
    @RequestMapping(value = "/userProto", consumes = "application/x-protobuf", produces = "application/x-protobuf")
    public UserResponse userProto(@RequestBody UserRequest request) {
        UserResponse userResponse = userServiceBlockingStub.query(request);
        String result = String.format("name:%s, age:%s, address:%s ", userResponse.getName(), userResponse.getAge(), userResponse.getAddress());
        System.out.println(result);
        return userResponse;
    }

}
