## 模块说明

- `spring-boot-grpc-client`：gRPC 客户端
- `spring-boot-grpc-proto`：protobuf 文件
- `spring-boot-grpc-server`：gRPC 服务端

## 使用说明

1. 进入到 `spring-boot-grpc-proto` 模块，运行 `mvn compile` 编译，通过 protobuf 文件，生成 Service 和 Message 类
2. 进入到 `spring-boot-grpc-server` 模块，运行启动类，启动 gRPC 服务端
3. 进入到 `spring-boot-grpc-client` 模块，运行启动类，启动 gRPC 客户端
